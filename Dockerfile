FROM node as build

RUN apt-get update && apt-get install -y --no-install-recommends \
    build-essential \
    libpng-dev \
    git \
    python3-minimal \
    libvhdi-utils \
    lvm2 \
    cifs-utils
RUN git clone -b master https://github.com/vatesfr/xen-orchestra && printf "[redis]\nuri = 'redis://redis'\n" >> /xen-orchestra/packages/xo-server/config.toml
RUN cd /xen-orchestra && yarn && yarn build

FROM node

COPY --from=build /xen-orchestra /xen-orchestra
CMD [ "sh", "-c", "cd /xen-orchestra/packages/xo-server && yarn start"]
