# xoa-docker-compose

docker compose application for xoa server

## Usage
To Start:
```
docker compose up
```

To Stop:
```commandline
docker compose stop
```

To wipe it out (like for a fresh build):
```
docker compose down -t 0
```

To Access:
```
http://localhost:8000
```

I just store my passwords for the connection in the browser (both login and xcp-ng server). I only spin up the container when i need to use it. 

## Roadmap
- add a persistent volume that will persist connections to servers across fresh builds
- 
## Contributing
Open to contribution
## Authors and acknowledgment
Robert Bush
